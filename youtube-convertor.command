#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DIR_OUT="$DIR/%(title)s.%(ext)s' "
echo "your path ---" $DIR
echo "Please enter a number (1 or 2)"
echo "1. MP3"
echo "2. MP4"
echo -e "\n"
read -n1 fileType doit

TYPE_MP3="1"
echo -e "\n"
if [ $fileType == $TYPE_MP3 ]; then
	echo "The file will be converted to MP3"
 else
    echo "The file will be converted to MP4"
fi

read -p "Enter youtube URL and press 'Enter': " URL

if [ $fileType == $TYPE_MP3 ]; then
	youtube-dl --output $DIR_OUT --extract-audio --audio-format mp3 --audio-quality 0 $URL
 else
   youtube-dl --output $DIR $URL
fi
echo "FINISHED"